const MongoClient = require('mongodb').MongoClient
const assert = require('assert')

// Connection URL
const url = 'mongodb://localhost:27017'

// Database Name
const dbName = 'school'

// Create a new MongoClient
const client = new MongoClient(url)

// Use connect method to connect to the Server
client.connect(async err => {
	assert.equal(null, err)
	console.log('Connected successfully to server')

	const db = client.db(dbName)

	const cursor = db.collection('grades').find({})
	cursor
	cursor.skip(6)
	cursor.limit(2)
	cursor.sort({ grade: 1 })
	cursor.project({ _id: 0, student: 1 })

	// 1 вариант
	const result = await cursor.toArray()
	console.log(result)

	// 2 вариант
	// cursor.forEach(
	// 	elem => {
	// 		console.log(elem.student)
	// 	},
	// 	err => {
	// 		if (err) console.warn(err)
	// 		return client.close()
	// 	}
	// )

	client.close()
})
